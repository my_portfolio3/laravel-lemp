<?php

use App\Http\Controllers\API\v1\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('/v1/auth/register', [AuthController::class, 'register']);
Route::post('/v1/auth/login', [AuthController::class, 'login']);


// Route::get('/user', function (Request $request) {
//     return $request->ip();
// })/* ->middleware('auth:sanctum') */;

// Route::fallback(fn () => "error");
