<?php

namespace App\DTO\User;

final class UserLoginDto
{
    public function __construct(
        public readonly string $email,
        public readonly string $password
    ) {
    }

    public function toArray(): array
    {
        return get_object_vars($this);
    }
}
