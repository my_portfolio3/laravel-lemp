<?php

namespace App\Http\Requests\Auth;

use App\DTO\User\UserLoginDto;
use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'email' => ['required', 'email'],
            'password' => ['required', 'string', 'max:255'],
        ];
    }

    public function getDto(): UserLoginDto
    {
        return new UserLoginDto(
            $this->email,
            $this->password
        );
    }
}
