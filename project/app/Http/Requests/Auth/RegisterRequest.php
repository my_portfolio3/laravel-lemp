<?php

namespace App\Http\Requests\Auth;

use App\DTO\User\UserRegisterDto;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class RegisterRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required', 'string', 'max:255'],
        ];
    }

    public function getDto(): UserRegisterDto
    {
        return new UserRegisterDto(
            $this->name,
            $this->email,
            Hash::make($this->password)
        );
    }
}
