<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Actions\User\UserCreateAction;
use App\Http\Requests\Auth\LoginRequest;
use App\Actions\User\UserTokenCreateAction;
use App\Http\Requests\Auth\RegisterRequest;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function register(RegisterRequest $request, UserCreateAction $action): Response
    {
        $user = $action($request->getDto());
        return response()->json($user);
    }

    public function login(LoginRequest $request, UserTokenCreateAction $action): Response
    {
        $token = $action($request->getDto());

        return response()->json([
            "token" => $token,
            "user" => $request->email,
        ]);
    }
}
