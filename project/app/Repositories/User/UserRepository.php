<?php

namespace App\Repositories\User;

use App\Models\User;
use App\DTO\User\UserLoginDto;
use App\DTO\User\UserRegisterDto;

final class UserRepository
{
    public function create(UserRegisterDto $dto): User
    {
        return User::create($dto->toArray());
    }    
    
    public function getByEmail(UserLoginDto $dto): User
    {
        return User::firstWhere("email", $dto->email);
    }
}
