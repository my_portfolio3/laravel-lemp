<?php

namespace App\Actions\User;

use App\Models\User;
use App\DTO\User\UserRegisterDto;
use App\Repositories\User\UserRepository;

final class UserCreateAction
{
    public function __construct(private UserRepository $repository)
    {
    }

    public function __invoke(UserRegisterDto $dto): User
    {
        return $this->repository->create($dto);
    }
}
