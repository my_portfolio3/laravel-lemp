<?php

namespace App\Actions\User;

use Illuminate\Support\Facades\Hash;
use App\DTO\User\UserLoginDto;
use App\Repositories\User\UserRepository;

final class UserTokenCreateAction
{
    public function __invoke(UserLoginDto $dto): string
    {
        $user = (new UserRepository())->getByEmail($dto);

        if (!$user || !Hash::check($dto->password, $user->password)) {
            return response()->json(['The provided credentials are incorrect.']);
        }

        $user->tokens()->delete();

        return $user->createToken($dto->email)->plainTextToken;
    }
}
